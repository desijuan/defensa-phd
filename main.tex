\documentclass[10pt]{beamer}

\usepackage{tikz-cd}
\usepackage[all,2cell]{xy}
\UseAllTwocells

\usepackage{amsmath,amssymb,amsthm,mathtools,tensor,bm}
\usepackage{stmaryrd} % para \mapsfrom
\mathtoolsset{showonlyrefs}
% \renewcommand*{\proofname}{Sketch of Proof}

\DeclareRobustCommand{\2cell}[5]{\xymatrix{#2 \ar@{}[r]|{\Downarrow #5}& #1 \ar@/^.7pc/[l]^{#4} \ar@/_.7pc/[l]_{#3}}}

\newcommand{\curly}{\mathrel{\leadsto}}

\newcommand\p{\pause}
\newcommand\ps{\smallskip}
\newcommand\ms{\medskip}
\newcommand\bs{\bigskip}

\newcommand\ro{\textcolor{red}}
\newcommand\az{\textcolor{blue}}

\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\g}{\mathfrak{g}}
\newcommand{\U}{\mathcal{U}}
\newcommand{\from}{\leftarrow}
\newcommand{\To}{\Rightarrow}
\newcommand{\From}{\Leftarrow}
\newcommand{\Iff}{\Leftrightarrow}
\newcommand{\xto}{\xrightarrow}
\newcommand{\xfrom}{\xleftarrow}
\newcommand{\dashto}{\dashrightarrow}
\newcommand{\tto}{\rightrightarrows}
\newcommand{\acts}{\curvearrowright}
\newcommand{\res}[2]{\left.#1\right|_{#2}}

\DeclareMathOperator{\Ker}{ker}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\im}{Im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\rk}{rk}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\Iso}{Iso}

\DeclareRobustCommand{\2cell}[5]{\xymatrix{#2 \ar@{}[r]|{\Downarrow #5}& #1 \ar@/^.7pc/[l]^{#4} \ar@/_.7pc/[l]_{#3}}}

\newtheorem{proposition}{Proposition}

\renewcommand{\qedsymbol}{}

%%%%%%%%%%%%%%%%%%%%
\title{Stacky Vector Bundles and Generalized Grassmannians}
\author{Juan Desimoni}
\institute{Universidade Federal Fluminense}
\date{15 december 2023}
%%%%%%%%%%%%%%%%%%%%

% \AtBeginDocument{\centering}

\begin{document}

%% Tapa
\begin{frame}
  \maketitle

  \centering
  Advisor: Matías L. del Hoyo
\end{frame}

%% Intro
\begin{frame}
  \vspace{-.25em}
  \begin{theorem}[Classification of Vector Bundles]
    \ps
    $M$ manifold, $k>0$.
    \vspace{-.25em}
    \[ VB_k[M] \quad \rightleftharpoons \quad \left\{ \text{maps of stacks} \ [M]\to [GL_k] \right\} \]
  \end{theorem} \p

  \vspace{-1ex}
  $E\to M$ a rank $r$ vector bundle, \p

  \ps
  a trivialization over an open cover $\U=\coprod U_i\to M$ defines a cocycle $\{\theta_{ji}\colon U_{ji}\to GL_r\}$ with values on $GL_k$
  \p
  $$ (M\tto M) \xfrom\sim \Big(\coprod U_{ji}\tto \coprod U_i\Big)\xto\theta (GL_r\tto\ast) $$
  \p

  \vspace{-1ex}
  Conversely, any cocycle $\{\theta_{ji}:U_{ji}\to GL_r\}$ can be used to build a rank $r$ vector bundle
  \vspace{-1ex}
  $$ \coprod U_i\times \RR^r/_{((x,i),v)\sim((x,j),\theta_{ji}(x)(v))}\to M $$
  \p

  \vspace{-1ex}
  \az{Classifying stack} $[GL_r]$,
  \p

  \ps
  cohomology ring $H_{dR}(GL_r)$: universal algebra of \az{characteristic classes}.
  \p

  \ms
  \textbf{Goal:} Generalize to Vector Bundles over Lie groupoids.

\end{frame}

%% VB Groupoids 1
\begin{frame}
  \frametitle{VB-groupoids}

  \( (\Gamma\tto E)\to(G\tto M) \qquad
  \begin{tikzcd}[ampersand replacement=\&, sep=small, cells={text width={width("$M$")}}, align=center]
    \Gamma \ar[yshift=3pt]{r} \ar[yshift=-2.3pt]{r} \ar{d} \& E \ar{d} \\
    G \ar[yshift=3pt]{r} \ar[yshift=-2.3pt]{r} \& M
  \end{tikzcd} \qquad\qquad\qquad
  \)\p

  \vspace{-.5ex}
  \az{Unit bundle:} $E\to M$\p

  \ms
  \az{Core:} $C\to M$ \quad $C=\ker(s\colon\res{\Gamma}{M}\to E)$\p

  \ms
  \textbf{Examples:}
  \begin{itemize}
    \item 2-vect (Baez, Crans): VB-groupoid over the point $*\tto *$.
      % \(
      %   \begin{tikzcd}[sep=small, cells={text width={width("$M$")}}, align=center]
      %     V_1 \ar[yshift=3pt]{r} \ar[yshift=-2.3pt]{r} \ar{d} & V_0 \ar{d} \\
      %     * \ar[yshift=3pt]{r} \ar[yshift=-2.3pt]{r} & *
      %   \end{tikzcd}
      % \)
    \item Tangent VB-groupoid: $(TG\tto TM)\to (G\tto M)$.
    \item Representations: $(G\tto M)\acts (E\to M)$, \ $G\times_M E\tto E$.
  \end{itemize}\p

  \bs
  \az{Acyclic VB-groupoids:} anchor map $\partial=\res{t}{C}: C\to E$ is a fiberwise isomorphism,

  \ms
  equiv, the projection $\Gamma\to 0_G$ is a quasi-isomorphism.\p

  \vspace{2ex}
  Determined up to isomorphism by $G\tto M$ and $E\to M$.\p

  \[ \left\{ \text{Reprs of Lie gpds} \right\} \quad \rightleftharpoons \quad \left\{ \text{VB-gpds trivial core} \right\} \]

\end{frame}

%% VB Groupoids 2
\begin{frame}
  \frametitle{VB-groupoids}

  $(\Gamma\tto E)\to(G\tto M)$ \quad VB-groupoid\p

  \bs
  canonical identification $t^*C = \ker(s:\Gamma\to E)$\p

  \bs
  \az{Cleavage:} linear section $\Sigma$ of the source map $\bar s:\Gamma\to s^*E$ over $G$.

  \ps
  \begin{equation}
    \begin{tikzcd}[ampersand replacement=\&, column sep=1.5em, row sep=small]
      0 \ar{r} \& t^*C \ar{r} \& \Gamma \ar{r}{\bar s} \& s^*E \ar{r} \ar[bend left = 50]{l}{\Sigma} \& 0
    \end{tikzcd}
  \end{equation}\p

  \ms
  \az{flat:} $\Sigma(h, t\Sigma(g, e))\Sigma(g, e) = \Sigma(hg, e)$\p

  \vspace{2ex}
  \az{unital:} $\Sigma(u_x, e) = u_e$ for all $x\in M$ and $e\in E_x$\p

  \ms
  \begin{lemma}
    Every VB-groupoid admits an unital cleavage.
  \end{lemma}

\end{frame}

%% Representations up to Homotopy
\begin{frame}
  \frametitle{Representations up to homotopy}\p

  \ms
  $E_1\oplus E_0\to M$ 2-term graded vector bundle, \
  $G\tto M$ Lie groupoid.\p

  \bs
  \az{2-term RUTH:} \ $(\partial,\rho,\gamma)\colon (G\tto M)\acts (E\to M)$\p
  \begin{itemize}
    \item Fiberwise linear map $\partial\colon E_1\to E_0$.\p
    \item Pseudo-representations $\rho_1\colon (G\tto M)\acts (E_1\to M)$ and $\rho_0\colon (G\tto M)\acts (E_0\to M)$ commuting with $\partial$.\p
    \item Homotopy \smash{$\gamma\colon (g_2, g_1)\in N_2G \mapsto \gamma^{g_2, g_1}\colon E_0^{s(g_1)}\to E_1^{t(g_2)}$} \\
    between $\rho^{g_2g_1}$ and $\rho^{g_2}\rho^{g1}$.
  \end{itemize}\p

  \vspace{-1em}
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      0 \ar{r} \& E_1^x \ar{r}{\partial^x} \ar[swap]{d}{\rho_1^g} \& E_0^x \ar{r} \ar{d}{\rho_0^g} \& 0 \\
      0 \ar{r} \& E_1^y \ar{r}{\partial^y} \& E_0^y \ar{r} \& 0
    \end{tikzcd}
    \quad
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      0 \ar{r} \& E_1^x \ar{r}{\partial_x} \ar[swap]{d}{\rho_1^{g_2}\rho_1^{g_1} - \rho_1^{g_2g_1}} \& E_0^x \ar{r} \ar{d}{\rho_0^{g_2}\rho_0^{g_1} - \rho_0^{g_2g_1}} \ar[sloped]{ld}{\gamma^{g_2, g_1}} \& 0 \\
      0 \ar{r} \& E_1^z \ar{r}{\partial_y} \& E_0^z \ar{r} \& 0
    \end{tikzcd}
  \]\p

  \textbf{Examples:} representations, manifolds, Lie groups,\\ adjoint represenation.\p

  \ms
  \begin{theorem}[GS-M, 2017]
    \vspace{-.5em}
    \[ \left\{ \txt{2-term RUTHs \\ $G\acts E_1\oplus E_0$} \right\} \quad \rightleftharpoons \quad \left\{ \txt{VB-groupoids \\ $\Gamma\to G$} \right\} \]
  \end{theorem}
\end{frame}

%% Stacky Vector Bundles 1
\begin{frame}
  \frametitle{Stacky Vector Bundles}

  $\phi,\psi\colon\Gamma\to\Gamma'$ morphisms of VB-groupoids over $G\tto M$.\p

  \ms
  \az{Linear equivalence:}
  $\alpha\colon\phi\Rightarrow\psi$ \p

  \ps
  vector bundle map covering $\text{id}_M$
  \[ \alpha\colon E\to\res{\Gamma'}{M} \qquad e\mapsto\psi(e)\xfrom{\alpha_e}\phi(e)\in\res{\Gamma'}{M} \]
  such that $\alpha_{\tilde e} \phi(\gamma) = \psi(\gamma) \alpha_e$ \ $\forall \tilde e\xfrom{\gamma} e\in \Gamma$.\p

  \bs
  Category of VB-groupoids: VB($G\tto M$)\p

  \ms
  \az{VB-groupoid derived category:} VB[$G\tto M$]\p

  \ms
  Obtained by identifying equivalent morphisms.\p

  \ms
  \textbf{Remark:} An isomorphism in VB[$G\tto M$] is a morphism that admits an inverse up to linear equivalence.\p

  \begin{theorem}[dH-O, 2020]
    If $\phi\colon(H\tto N)\to(G\tto M)$ is Morita, \\
    then $\phi^*\colon VB[G\tto M]\to VB[H\tto N]$ is an equivalence.
  \end{theorem}

\end{frame}

%% Stacky Vector Bundles 2
\begin{frame}
  \frametitle{Stacky Vector Bundles}

  $(\Gamma\tto E)\to(G\tto M)$ \ VB-groupoid, \p\ $\partial\colon C\to E$ anchor map,\p

  \bs
  $p=\rk C$ and $q=\rk E$.\p

  \bs
  \az{homological rank:} ${\rm hrk}\Gamma =(p',q')=(p-r,q-r)$,

  \ms
  $r$ is the maximum rank of $\partial^x$, as $x$ runs through $M$.\p

  \bs
  Writing $H_1^x(\Gamma)=\ker\partial^x$ and $H_0^x(\Gamma)=\coker\partial^x$, $p'$ is the maximal dimension of $H_1^x(\Gamma)$ and $q'$ is the maximal dimension of $H_0^x(\Gamma)$.\p

  \bs
  \textbf{Remark:} if $\phi:\Gamma\to\Gamma'$ is a linear equivalence, then the homological ranks of $\Gamma$ and $\Gamma'$ agree.\p\
  Well defined \az{invariants}.\p

  \bs
  Homological rank should be considered as the \az{derived analog} to the rank.

\end{frame}

%% Lie 2-groupoids 1
\begin{frame}
  \frametitle{Lie 2-groupoids}

  \az{Objects:} $G_0\colon$ $x$,
  \p\
  \az{Arrows:} $G_1\colon$ $y\xfrom{g}x$,
  \p\
  \az{2-cells:} $G_2\colon$ $\2cell{x}{y}{f}{g}{\alpha}$
  \p

  \az{Morphisms:} Lax functors: $\phi\colon G\to H$.\p \ Three functions $\phi_i\colon G_i\to H_i$ preserving the source, target, units and the vertical composition $\bullet$.
  The horizontal composition $\circ$ is preserved only up to a given 2-cell.
  \[ \phi_{1,1}\colon G_1\times_{G_0}G_1\to H_2 \qquad \phi_{1,1}(g,f)\colon\phi_1(gf)\To\phi_1(g)\circ\phi_1(f) \]\p

  \vspace{-2ex}
  \begin{definition}
    A \textbf{Lie 2-groupoid} $G$ is a 2-groupoid
    $G_2\tto G_1\tto G_0$
    such that:
    \begin{enumerate}[{L2}a)]
      \item objects $G_0$, arrows $G_1$ and 2-cells $G_2$ are manifolds,
      \item the source and target $s,t\colon G_1\to G_0$ and  $s,t\colon G_2\to G_1$ are surjective submersions,
      \item the horizontal and vertical compositions $\circ\colon G_2\times_{G_0} G_2\to G_2$ and $\bullet\colon G_2\times_{G_1} G_2\to G_2$, units $u\colon G_0\to G_1$, $u\colon G_1\to G_2$, and inversion of 2-cells $i\colon G_2\to G_2$ are smooth, and
      \item the restriction maps $d_{2,0}\colon N_2G\to N_{2,0}G$ and $d_{2,2}\colon N_2G\to N_{2.2}G$ are surjective submersions.
    \end{enumerate}
  \end{definition}

\end{frame}

%% General Linear 2-Groupoid 1
\begin{frame}
  \frametitle{The General Linear 2-Groupoid}

  \ms
  $V=V_1\oplus V_0\to M$ \ 2-term graded vector bundle.\p

  \ms
  \textbf{Objects:} (linear) differentials $\eta^x$ on the fibers of $V$.
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      0 \ar{r} \& V_1^x \ar{r}{\eta^x} \& V_0^x \ar{r} \& 0
    \end{tikzcd}
  \]

  \p

  \ps
  \textbf{Arrows:} pair of linear maps $\alpha=(\alpha_1,\alpha_0)$ defining a quasi-iso.
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      0 \ar{r} \& V_1^x \ar{r}{\eta^x} \ar{d}{\alpha_1} \& V_0^x \ar{r} \ar{d}{\alpha_0} \& 0 \\
      0 \ar{r} \& V_1^y \ar{r}{\nu^y} \& V_0^y \ar{r} \& 0
    \end{tikzcd}
  \]

  \p

  \ps
  \textbf{2-cells:} homotopies $R\colon\alpha\Rightarrow\beta$.
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      0 \ar{r} \& V_1^x \ar{r}{\eta^x} \ar[swap]{d}{\alpha_1-\beta_1} \& V_0^x \ar{r} \ar{d}{\alpha_0-\beta_0} \ar[swap]{dl}{R} \& 0 \\
      0 \ar{r} \& V_1^y \ar{r}{\nu^y} \& V_0^y \ar{r} \& 0
    \end{tikzcd}
  \]

  \p

  \textbf{Horizontal composition:} composition of maps.

  \textbf{Vertical composition:} composition of chain homotopies.\p

  \ms
  \az{Notation:} $GL_{pq} = GL(\RR^p\oplus\RR^q)$
  \bs\bs

\end{frame}

%% General Linear 2-Groupoid 2
\begin{frame}

  \begin{theorem}[dH-S, 2019]
    $V=V_1\oplus V_0\to M$ \ 2-term graded vector bundle,

    \ps
    $GL(V_1\oplus V_0)$ is a Lie 2-groupoid.
  \end{theorem}\p

  \vspace{-1ex}
  \[
    \left\{ \txt{morphisms \\ $G\to GL(C\oplus E)$} \right\}
    \quad \rightleftharpoons \quad
    \left\{ \txt{2-term RUTHs \\ $G\acts C\oplus E$} \right\}
    \p
    \quad \rightleftharpoons \quad
    \left\{ \txt{VB-groupoids \\ $\Gamma\to G$} \right\}
  \]\p

  \vspace{7ex}
  \begin{theorem}[Structure Theorem]
  Let $G$ be a Lie 2-groupoid. Then:
    \begin{enumerate}[ST1)]
      \item Orbits $O_x=\{y\mid\exists y\from x\}\subset G_0$ are a submanifolds, non-necessarily embedded.
      \item Local groupoids $G(y,x)=(G_2(y,x)\tto G_1(y,x))$ are Lie groupoids. In particular, $G(x,x)$ are Lie 2-groups.
    \end{enumerate}
  \end{theorem}

\end{frame}

%% Morita Morphisms 1
\begin{frame}
  \frametitle{Morita morphisms}

  $\phi\colon (G\tto M)\to (H\tto N)$ morphism of Lie groupoids\p

  \bs
  \az{Morita}: smoothly essentially surjective and (set-theoretically) fully faithful.\p

  \ms
  \begin{definition}
    $\phi\colon (G\tto M)\to (H\tto N)$ is \az{Morita} if:
    \p
    \ms
    \begin{enumerate}[M1)]
      \item $\phi_0\colon M\to N$ meets transversely every orbit, namely

        \ms
        $M\to N\to N/H$ and $T_xM\to T_{\phi(x)}N\to N_{\phi(x)}O$ are surjective.
        \p

        \ms
      \item $\phi$ is locally bijective, namely the function

        \ms
        $\phi_{y,x}\colon G(y,x)\to H(\phi(y),\phi(x))$ is a bijection $\forall x,y$.

    \end{enumerate}
  \end{definition}

\end{frame}

%% Morita Morphisms 2
\begin{frame}

  \begin{proposition}
  The following are equivalent:\p
  \begin{enumerate}[a)]
  \item $\phi$ is Morita.\p
  \item $\phi$ induces a homeomorphism \smash{$\overline\phi\colon M/G\to M'/G'$} between the orbit spaces, isomorphisms \smash{$\overline{d_x\phi}\colon N_xO\to N_{\phi(x)}O$} between the normal spaces, and isomorphisms \smash{$\phi_x\colon G_x\to G'_{\phi(x)}$} between the isotropy groups.\p
  \item $t\pi_1\colon G'\times_{M'} M\to M'$ is a surjective submersion and the following is a manifold-theoretic fibered product:
  \vspace{-1ex}
    \begin{equation}
      \begin{tikzcd}[ampersand replacement=\&]
        G \ar{r}{\phi} \ar{d}[swap]{(t, s)} \& G' \ar{d}{(t, s)} \\
        M\times M \ar{r}{\phi\times\phi} \& M'\times M'
      \end{tikzcd}
    \end{equation}
  \end{enumerate}
  \end{proposition}\p

  \vspace{-2ex}
  \begin{proof}\p
    b) $\Iff$ c) Shown in paper by M. del Hoyo from 2013.\p

    a) $\To$ c) The bijection $\phi_{y,x}\colon G(y,x)\to H(\phi(y),\phi(x))$ is equivariant\p

    \quad $\To$ it is a diffeomorphism\p

    \quad $\To$ The map $(\phi,t,s)\colon G\to H\times_{N\times N} (M\times M)$ is a diffeomorphism.
    % A vector $v\in T_gG$ in the kernel of the differential of $(\phi,t,s)$ is both in the kernel of the differentials of $\phi$ and the anchor map $(t,s)$.
    % The last condition implies that $v$ is actually in $T_gG(x',x)$ and the first implies that it must be zero.
    % The bijection $(\phi,t,s)\colon G\to H\times_{N\times N} (M\times M)$ turns out to be an immersion, therefore a diffeomorphism.
  \end{proof}

\end{frame}

%% Equivalences 1
\begin{frame}
  \frametitle{Equivalences}\p

  $G$, $H$ Lie 2-groupoids, $M$ manifold, $\psi\colon M\to H_0$  smooth map.\p

  $\psi$ \az{meets transversally every orbit} of $H$:\p

  $M\to H_0/H$ is surjective and $T_xM\to N_{\psi(x)}H_0$ is surjective $\forall x\in M$\p

  $\phi\colon G\to H$ morphism of Lie 2-groupoids.\p

  \az{Locally Morita:}\p

  $\forall x, y\in G_0$ the restriction to the hom-groupouid
  \[ \phi_{x,y}\colon \left( G_2(y,x)\tto G_1(y,x) \right) \to \left( H_2(\phi(y),\phi(x))\tto H_1(\phi(y),\phi(x)) \right) \]
  is Morita.\p

  \begin{definition}
    $\phi$ is an \textbf{equivalence} if:
    \begin{enumerate}[E1)]
      \item $\phi_0\colon G_0\to H_0$ meets transverselly every orbit; and
      \item $\Phi$ is locally Morita.\p\
    \end{enumerate}
    If the map on the objects $\phi_0$ is a surjective submersion, $\phi$ is a \textbf{Surjective equivalence}.
  \end{definition}\p

  \ms
  For Lie 1-groupoids: equivalence = Morita morphism.

\end{frame}

%% Equivalences 2
\begin{frame}

  $GL'_{pq}\subset GL_{pq}$ Lie 2-subgroupoid of invertible arrows.\p

  \begin{lemma}
    The inclusion $i:GL_{pq}' \to GL_{pq}$, is a surjective equivalence.
  \end{lemma}\p

  \vspace{2ex}
  Consider $U^r_{pq}=\{\partial\in (GL_{pq})_0 \mid \rk\partial\geq r\}\subset (GL_{pq})_0$.\p

  \bs
  $GL^r_{pq}\subset GL_{pq}$ Lie 2-groupoid obtained by restricting $GL_{pq}$ to $U^r_{pq}$.\p

  \bs
  $U^0_{pq}=(GL_{pq})_0$, $U^1_{pq}=(GL_{pq})_0\setminus\{0\}$ and $U^r$ is empty for any $r > r_0 \coloneqq \min(p,q)$.

  $$ (GL_{pq})_0 = U^0_{pq} \supset U^1_{pq} \supset \dots \supset U^r_{pq} \supset \dots \supset U^{r_0}_{pq} \supset U^{r_0+1}_{pq} = \emptyset $$\p

  \vspace{-6ex}
  \begin{align}
    \lambda:GL_{pq} &\to GL_{p+1,q+1} \\
    \partial &\mapsto \lambda(\partial)=\partial\oplus 1 \\
    \rho &\mapsto \lambda(\rho)=\rho\oplus 1 \\
    \gamma &\mapsto \lambda(\gamma)=\gamma\oplus 0
  \end{align}\p

  \vspace{-6ex}
  \begin{proposition}
    $\lambda^r\colon GL_{pq}\to GL^r_{p+r,q+r} \subset GL_{p+r,q+r}$ is an equivalencefor every $r\geq0$.
  \end{proposition}

\end{frame}

%% Equivalences 3
\begin{frame}
  \az{Ginot-Stienon, 2015:} $\phi\colon G\to H$ strict morphism of strict Lie 2-groupoids, $\phi_0\colon G_0\to H_0$ surjective submersion
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      \& \phi_0^*H \ar{dr} \\
      G \ar{ur} \ar{rr}{\phi} \& \& H
    \end{tikzcd}
  \]
  \p
  \textbf{Equivalence:} $G_1\to \phi_0^*H_1$ surjective submersion and $(G_2\tto G_1)\to (\phi_0^*H_2\tto \phi_0^*H_1)$ Morita.\p

  \ms
  \az{Zhu, 2009:} $X$, $Y$ weak Lie 2-groupoids:
  simplicial manifolds in which every inner horn admits a filling and the filling is unique for $n>2$.
  $\phi\colon X\to Y$ morphism of weak Lie 2-groupoids.\p

  \textbf{Equivalence:} $X_n \to Y_n X \times_{S^\phi_n Y} S^\phi_n X$ surjective submersions for $0\leq n<2$ and diffeomorpism for $n=2$.\p

  \ms
  \begin{theorem}[Comparison Theorem]
    $\phi:G\to H$ morphism of Lie 2-gpds s. t. $\phi_0$ is a surjective submersion.
    \begin{enumerate}[CT1)]
      \item If $\phi$ is strict and $\phi_1:G_1\to\phi_0^*H_1$ are surjective submersions, then

        GS-equivalence $\Iff$ equivalence.
      \item  $\phi_1: G_1\to H_1$ surjective submersion, then

        $\phi$ equivalence $\Iff$ $N\phi$ hypercover.
    \end{enumerate}
  \end{theorem}

\end{frame}

%% 2-Stacks 1
\begin{frame}
  \frametitle{Differentiable 2-Stacks}
  \p
  \ms
  Lie 2-groupoids up to equivalence:
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep = small]
      G \& H \ar[shorten = -.15em]{l}[pos=.45, above]{\sim} \ar[shorten = -.15em]{r}[pos=.45]{\sim} \& G'
    \end{tikzcd}
  \]
  \p

  \ps
  \textbf{Fractions:} invert the equivalences.
  \[ (\psi, \phi)\colon
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      G \& H \ar[shorten = -.15em]{l}[pos=.45, sloped, below]{\sim}[pos=.45, sloped, above]{\phi} \ar[shorten = -.15em]{r}[pos=.45]{\psi} \& G'
    \end{tikzcd}
  \]
  \p

  Two pairs $(\psi_1, \phi_1)$ and $(\psi_2, \phi_2)$ are equivalent if there is third one $(\psi_3, \phi_3)$ that sits on top of the first two.
  \[
    \begin{tikzcd}[ampersand replacement=\&]
      \& H_1 \ar{dl}[pos=.4, sloped, below]{\sim}[pos=.4, sloped, above]{\phi_1} \ar{dr}[pos=.4, sloped, above]{\psi_1} \& \\
     G \& H_3 \ar{l}[pos=.4, sloped, below]{\sim}[pos=.4, sloped, above]{\phi_3} \ar{u}[pos=.4, sloped, above]{\sim} \ar{r}[pos=.4, sloped, above]{\psi_3} \ar{d}[pos=.4, sloped, below]{\sim} \& G' \\
      \& H_2 \ar{ul}[pos=.4, sloped, above]{\phi_2}[pos=.4, sloped, below]{\sim} \ar{ur}[pos=.4, sloped, above]{\psi_2} \&
    \end{tikzcd}
  \]
  \p
  \az{Map of 2-stacks:} $\psi/\phi\colon [G]\to [G']$ equivalence class of pairs $(\psi, \phi)$.
  \bs
\end{frame}

%% 2-Stacks 2
\begin{frame}
  \frametitle{Differentiable 2-Stacks}

  Given two maps of 2-stacks $[\psi/\phi]\colon [G]\to [G']$, $[\psi'/\phi']\colon [G']\to [G'']$,

  their {\bf composition} is defined as $[\psi'\psi''/\phi''\phi]\colon [G]\to [G'']$, where $\phi''\colon K\xto\sim H$ and $\psi'\colon K\to H'$ are such that the diagram below commutes up to isomorphism:
  \[
    \begin{tikzcd}[ampersand replacement=\&, sep=small]
      \& \& K \ar{dl}[pos=.4, sloped, below]{\sim}[above]{\phi''} \ar{dr}[pos=.3, yshift=-2pt]{\psi''} \& \& \\
      \& H \ar{dl}[pos=.4, sloped, below]{\sim}[pos=.55, above]{\phi} \ar{dr}[pos=.3, yshift=-2pt]{\psi} \& \& H' \ar{dl}[pos=.4, sloped, below]{\sim}[above]{\phi'} \ar{dr}[pos=.3, yshift=-2pt]{\psi'} \& \\
      G \& \& G' \& \& G''
    \end{tikzcd}
  \]\p

  \vspace{-2ex}
  \begin{proposition}
  The relation defined over fractions is an equivalence relation, and the composition of fractions is well-defined.
  \end{proposition}\p

  \ms
  \az{Key:} Homotopy fiber product.\p

  \ps
  \begin{definition}
    \textbf{Categorified Grassmannian:} $[GL_{pq}]$
  \end{definition}

\end{frame}

%% Good Representatives
\begin{frame}
  \frametitle{Good Representatives}

  \begin{proposition}
    $G = (G_1\tto M)$ Lie 1-groupoid seen as a Lie 2-groupoid with trivial 2-cells.
    Every map of 2-stacks $[G]\to [GL_{pq}]$ can be represented by a fraction of the form
    \[
      \begin{tikzcd}[ampersand replacement=\&, sep = small]
        G \& G_\U \ar{l}[pos=.45, above]{\sim} \ar{r} \& GL_{pq}
      \end{tikzcd}
    \]
    for some open cover $\U=\{U_i\}$ of $M$.
  \end{proposition}\p

  \begin{proof}
    $\phi/\pi\colon G\from H\to GL_{pq}$ representative of $[G]\to [GL_{pq}]$.\p

    Take local sections $\sigma\colon \coprod U_i\to H_0$ of $\pi_0\colon H_0\to M$,\p\
    let $H_\U = \sigma^* H$ be the pull-back via $\sigma$.\p
    \vspace{-1ex}
    \begin{equation}
      \begin{tikzcd}[ampersand replacement=\&]
        G_\U \ar{d}[sloped]{\sim} \& H_\U \ar{l}[below]{\sim}[above]{\pi} \ar{d}[sloped]{\sim} \ar{dr}{\hat\phi} \\
        G \& H \ar{l}[below]{\sim}[above]{\pi} \ar{r}[above]{\phi} \& GL_{pq}
      \end{tikzcd}
    \end{equation}\p

    \vspace{-1ex}
    \az{Key:} Twist the morphism $\hat\phi\colon H_\U\to GL_{pq}$ with an homotopy $h\colon (H_\U)_1\to (GL_{pq})_2$, to get another morphism $\psi=\phi^h$ which descends to the quotient $H_\U\to G_\U$.
  \end{proof}

\end{frame}

%% The Classification Theorem 1
\begin{frame}
  \frametitle{The Classification Theorem}

  Consider $\RR^q\xfrom 0\RR^p\in (GL_{pq})_0$.

  \begin{theorem}[Classification Theorem]
    $[G]$ stack, $p,q\geq 0$. There is a bijective correspondence
    $$ VB_{pq}[G] \quad \rightleftharpoons \quad \big\{ \text{ maps of 2-stacks $[G]\to [GL_{pq}]$ having $0$ in the image} \big\} $$
    between isomorphism classes of stacky vector bundles $[\Gamma]\to[G]$ of homological rank $(p,q)$ and maps of 2-stacks $[G]\to[GL_{pq}]$ having 0 in the image.
  \end{theorem}\p

  \ms
  \begin{theorem}[Classification Theorem, second version]
    $[G]$ stack, $p,q\geq 0$. There is a bijective correspondence
    $$ VB_{\prec pq}[G] \quad \rightleftharpoons \quad \big\{ \text{ maps of 2-stacks $[G]\to [GL_{pq}]$} \big\} $$
    between isomorphism classes of stacky vector bundles $[\Gamma]\to[G]$ of homological rank $(p',q')\prec(p,q)$ and maps of 2-stacks $[G]\to[GL_{pq}]$.
  \end{theorem}

\end{frame}

%% The Classification Theorem 2
\begin{frame}
  \frametitle{The Classification Theorem}

  \begin{proof}

    \ms
    $X\in VB_{pq}[G]$ stacky vector bundle of homological rank $(p,q)$ \p

    \bs
    $X = [\Gamma]$, \quad $\rk(C\to M) = p+r$, \quad $\rk(E\to M) = q+r$ \p

    \bs
    $ \begin{tikzcd}[ampersand replacement=\&, sep=small]
      G_\U \ar{r} \ar{d}[sloped]{\sim} \& GL(C_\U\oplus E_\U) \ar{r} \ar{d}[sloped]{\sim} \& GL_{p+r,q+r} \\
      G \ar{r} \& GL(C\oplus E)
    \end{tikzcd} $ \p

    \bs
    $ [\lambda^r]:[GL_{pq}]\to [GL^r_{p+r,q+r}] $ isomorphism \p

    \bs
    $ X \quad \mapsto \quad [\lambda^r]^{-1}\circ [\pi\phi^{\Sigma,\U}/\pi_\U]$

    $$ [G]\xto{[\pi\phi^{\Sigma,\U}/\pi_\U]}[GL^r_{p+r,q+r}]\xto{[\lambda^r]^{-1}} [GL_{pq}] $$

  \end{proof}
\end{frame}

%% Questions
\begin{frame}
  \frametitle{Questions / future work}\p

  \begin{itemize}
    \item Equivalence of categories?\p
    \item Cohomology of $GL_{pq}$? $\curly$ Characteristic Classes.\p
    \item Applications to Poisson and Symplectic Geometries, where Lie groupoids, VB-groupoids, and their associated stacks arise naturally.
  \end{itemize}\p

  \vspace{3em}
  \centering\Huge
  \az{Thank you!}

\end{frame}

\end{document}
