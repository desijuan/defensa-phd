build:
	pdflatex main.tex

clean:
	find . -name "main.*" ! -name "main.tex" -exec rm {} \;

.PHONY: build clean
